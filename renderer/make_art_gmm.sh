#!/bin/sh

git clone --depth 1 git://cgg.mff.cuni.cz/ART.git
cp gmm.patch ART
cd ART
git apply gmm.patch
