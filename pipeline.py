#!/usr/bin/env python3

import os, subprocess
import matplotlib.pyplot as plt
import seaborn as sns
from radiometry.fluospectrum import FluoSpectrum
from fitting.gmm import GMM, GMMBase
import config

def artist(scene, spectrum, n_samples, seed, output_name):
    subprocess.call(
        [config.art_dir + 'artist', scene,
        '-DSAMPLES='+str(n_samples),
        '-DSPECTRUM=' + str(spectrum),
        '-seed', str(seed),
        '-o', output_name,
         '-b',
         '-j', '15'
        ]
    )

if __name__ == '__main__':
    if not os.path.exists(config.output_dir):
        os.mkdir(config.output_dir)
    if not os.path.exists(config.reference_dir):
        os.mkdir(config.reference_dir)

    col_names = ['Reference'] + [str(n) + ' Gaussians' for n in config.n_gaussians]

    for scene, exposure in config.scenes:
        scene_name = os.path.splitext(os.path.basename(scene))[0]
        for s in config.spectra:
            print('Running for:', s)

            # Various paths for the reference dataset
            path_ref_spectrum   = os.path.join(config.asset_dir, 'fluorescence','fluorescent_{}'.format(s))
            path_ref_rendering  = os.path.join(config.reference_dir,'rendering-ref-{}-{}.{}'.format(s, scene_name, config.ext))

            # Load the dataset
            fluo_spectrum = FluoSpectrum(path_ref_spectrum)

            # Reference rendering
            if config.run_ref_rendering:
                spectrum_ref = s.upper() + '_POSTIT_FLUORESCENT'
                artist(scene, spectrum_ref, config.n_samples, config.seed, path_ref_rendering)
                #tonemap(path_ref_rendering, exposure, path_ref_rendering[:-len(config.ext)] + 'png')

            # Now, perform the comparison with various number of Gaussians
            for n in config.n_gaussians:
                print('  Gaussians:', str(n))

                # Various paths for the fitted dataset
                path_fit_spectum    = os.path.join(config.output_dir, '{}-{}.gmm'.format(s, n))
                path_fit_rendering  = os.path.join(config.output_dir, 'rendering-fit-{}-{}-{}.{}'.format(s, n, scene_name, config.ext))

                # Run the fit only if the file does not exist
                # otherwise, loads the fitted data
                if not os.path.exists(path_fit_spectum):
                    if(n==1): # pomgrenate doesn't support one component.
                        raise Exception('Wrong gaussians input')
                    else:
                        fitting = GMM(n, config.optim, fluo_spectrum)
                    fitting.save(path_fit_spectum)

                else:
                    fitting = GMMBase(path_fit_spectum)

                # Fitting rendering
                if config.run_fit_rendering:
                    spectrum_tab_cmp = 'GMM_FIT("{}")'.format(path_fit_spectum)
                    artist(scene, spectrum_tab_cmp, config.n_samples, config.seed, path_fit_rendering)
                    #tonemap(path_fit_rendering, exposure, path_fit_rendering[:-len(config.ext)] + 'png')
