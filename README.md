GMM Fluorescence Representation
===============================

This repository contains the code used in the EGSR 2021 paper *A
Compact Representation for Fluorescent Spectral Data* by Quingqin Hua,
Alban Fichet, Alexander Wilkie.

It is provided as it, without any waranty (see LICENSE.txt). Other
parts of this repository are licensed differently. Please report to
the respective license of the data originate from.

The pipeline on how to compact the fluorescence data into GMM
representation and perform the sampling in the spectral renderer.

| Folder       | Description                                                   |
|:-------------|:--------------------------------------------------------------|
| `asset`      | contains the sample fluorescent 2D re-radiation.              |
| `radiometry` | contains the general processing of the re-radation.           |
| `fitting`    | contains the method on fitting to the re-radiation and the reconstruction. |
| `renderer`   | contains a patch file to obtain the same source tree as the one used from the paper from a base ART sourcetree                                |

To run the pipeline, please run `pipeline.py`, which will give you the
visualization of the fitting and the rendering result.