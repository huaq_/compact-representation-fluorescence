#!/usr/bin/env python3

import numpy as np

class FluoSpectrum:
    wavelength_i_start = 0
    wavelength_i_end = 0
    wavelength_i_sampling = 1
    wavelength_i_n_samples = 0
    
    wavelength_o_start = 0
    wavelength_o_end = 0
    wavelength_o_sampling = 1
    wavelength_o_n_samples = 0

    data = np.zeros((wavelength_i_n_samples, wavelength_o_n_samples, 1))

    def __init__(self, filename):
        tmp_data = []

        with open(filename, 'r') as f:
            header_pos = 0

            for line in f:

                # Read header info
                if line.startswith('#'):
                    a, b = line[1:].split()
                    if header_pos == 0:
                        self.wavelength_i_n_samples = int(a)
                        self.wavelength_o_n_samples = int(b)
                    elif header_pos == 1:
                        self.wavelength_i_start = float(a)
                        self.wavelength_i_sampling = float(b)
                    elif header_pos == 2:
                        self.wavelength_o_start = float(a)
                        self.wavelength_o_sampling = float(b)
                    header_pos += 1
                else:
                    tmp_data.append([float(el) for el in line.split()])

            self.data = np.reshape(tmp_data, (self.wavelength_o_n_samples, self.wavelength_i_n_samples))
            
            self.wavelength_i_end = self.wavelength_i_start + (self.wavelength_i_n_samples - 1) * self.wavelength_i_sampling
            self.wavelength_o_end = self.wavelength_o_start + (self.wavelength_o_n_samples - 1) * self.wavelength_o_sampling

    def reradiation_idx(self, wl_idx_in, wl_idx_out):
        return self.data[wl_idx_out, wl_idx_out]

    def get_pure_fluo(self):
        if self.wavelength_i_sampling != self.wavelength_o_sampling:
            print('We do not support manipulation of reradiation matrix when')
            print('different sampling are used for input and output')

        start_wl = max(self.wavelength_i_start, self.wavelength_o_start)

        start_wl_i_idx = self.__idx_for_wl_in(start_wl)
        start_wl_o_idx = self.__idx_for_wl_out(start_wl)

        ret_array = self.data.copy()

        for idx_i, idx_o in zip(
                        range(start_wl_i_idx, self.wavelength_i_n_samples), 
                        range(start_wl_o_idx, self.wavelength_o_n_samples)):
            ret_array[idx_o, idx_i] = 0

        return ret_array
    
    def get_non_fluo(self):
        if self.wavelength_i_sampling != self.wavelength_o_sampling:
            print('We do not support manipulation of reradiation matrix when')
            print('different sampling are used for input and output')

        start_wl = max(self.wavelength_i_start, self.wavelength_o_start)

        start_wl_i_idx = self.__idx_for_wl_in(start_wl)
        start_wl_o_idx = self.__idx_for_wl_out(start_wl)

        ret_array = []

        for idx_i, idx_o in zip(
                        range(start_wl_i_idx, self.wavelength_i_n_samples), 
                        range(start_wl_o_idx, self.wavelength_o_n_samples)):
            ret_array.append(self.data[idx_o, idx_i])

        return ret_array

    def gnuplot_reradiation(self, output):
        rerad = self.get_pure_fluo()
        
        with open(output, "w") as f:
            for i in range(rerad.shape[0]):
                for o in range(rerad.shape[1]):
                    if rerad[i, o] > 0:
                        f.write('{} '.format(rerad[i, o]))
                    else:
                        f.write('0 ')
                f.write('\n')

    def tikz_reradiation(self, output):
        rerad = self.get_pure_fluo()
        wavelength_i = self.get_illumination_wavelengths()
        wavelength_o = self.get_reflectance_wavelengths()

        with open(output, "w") as f:
            for i in range(rerad.shape[1]):
                wl_i = wavelength_i[i]
                for o in range(rerad.shape[0]):
                    wl_o = wavelength_o[o]
                    if rerad[o, i] > 0:
                        f.write('{} {} {}\n'.format(wl_i, wl_o, rerad[o, i]))
                    else:
                        f.write('{} {} 0\n'.format(wl_i, wl_o))
                f.write('\n')

    def get_illumination_wavelengths(self):
        return np.linspace(self.wavelength_i_start, self.wavelength_i_end, self.wavelength_i_n_samples)

    def get_reflectance_wavelengths(self):
        return np.linspace(self.wavelength_o_start, self.wavelength_o_end, self.wavelength_o_n_samples)

    def __idx_for_wl_in(self, wl_in):
        return int(np.floor(wl_in - self.wavelength_i_start) // self.wavelength_i_sampling)

    def __idx_for_wl_out(self, wl_out):
        return int(np.floor(wl_out - self.wavelength_o_start) // self.wavelength_o_sampling)
