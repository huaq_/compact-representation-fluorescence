Fluorescent data
================

These files are extracted from the ART codebase and were provided by
Labsphere Inc. The original files in their original form are
accessible from
`<prefix>/lib/ART_Resources/SpectralData/FluorescentPostIt.ark`.

If you want to use these file in your project, you shall reference the
original source of thoses data. We are not the original providers of
those measurements.