# Global setting for the pipeline
import os

# Output folder for fitted gmm parameters
output_dir = 'output'
# Output folder for rendering reference
reference_dir = os.path.join('reference')
# Fluorescence data and scene path
asset_dir = os.path.join('assets')

# Specify the ART binary
# To get ART binary with compacted GMM, see instructions under "renderer" 
art_dir = ''

# Rendering parameters
n_samples = 64
seed = 512
ext = 'artraw'
run_ref_rendering = False
run_fit_rendering = True

# Scene Assets
# SimpleScene: the scene with day light
# SceneUV: the scene with near UV light 
scenes = [
        (os.path.join('assets','scenes', 'SimpleScene.arm'), 1),
        (os.path.join('assets','scenes', 'SceneUV.arm'), 16)
        ]
        
# Fitting parameters
spectra = ['green', 'pink', 'yellow']
n_gaussians = [2, 6]
# Weight for the minimisation process
optim = 'MSE' #'MSE_weighted', 'L1', 'integral'